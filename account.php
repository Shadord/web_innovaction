<?php
	include "bdd.php";
	include "QR/qrlib.php";

	$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
	$PNG_WEB_DIR = DIRECTORY_SEPARATOR.'web_innovaction\temp'.DIRECTORY_SEPARATOR;

	function generate_string($input, $strength = 16) {
	    $input_length = strlen($input);
	    $random_string = '';
	    for($i = 0; $i < $strength; $i++) {
	        $random_character = $input[mt_rand(0, $input_length - 1)];
	        $random_string .= $random_character;
	    }

	    return $random_string;
	}


	if(isset($_GET['logout'])) {
		if(strcmp($_GET['logout'], "true") == 0) {
			session_destroy();
			header('Location: login.php');
		}
	}
	if(isset($_SESSION['user_id'])) { // Si ya un parametre
		$user_id = $_SESSION['user_id'];
		$query=$bdd->prepare('SELECT * FROM users WHERE user_id = :cv');
		$query->bindValue(':cv',$user_id, PDO::PARAM_STR);
		$query->execute();
		$user=$query->fetch();
		if(isset($user['user_id'])) { // Si il est valide
			if(isset($_POST['object_QR_code_name'])) {
				$name = htmlspecialchars($_POST['object_QR_code_name']);
				$code_token = generate_string($permitted_chars, 24);
				$QR_code_value = "http://findit.ddns.net/web_innovaction/find.php?code_token=".$code_token;
				$errorCorrectionLevel = 'H';
				$matrixPointSize = 7;
				$filename = $PNG_TEMP_DIR.'test'.md5($code_token.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
				$webfilename = $PNG_WEB_DIR.'test'.md5($code_token.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
				QRcode::png($QR_code_value, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
				$query=$bdd->prepare('INSERT INTO `qr_code`(`code_owner_id`, `code_value`, `code_active`, `code_name`, `code_url`) VALUES (:own,:value,1,:name,:url)');
				$query->bindValue(':own',$user_id, PDO::PARAM_STR);
				$query->bindValue(':value',$code_token, PDO::PARAM_STR);
				$query->bindValue(':name',$name, PDO::PARAM_STR);
				$query->bindValue(':url',$webfilename, PDO::PARAM_STR);
				$query->execute();
				header('Location: account.php');
			}

		}else{// Si le param est invalide
			header('Location: 404.php');
		}
	}else{ // SI ya pas de param
		header('Location: 404.php');
	}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Find It - Account</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">


<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100" style="background-color: #42A5F5;">
			<div class="wrap-login100 p-l-45 p-r-45 p-t-65 p-b-54">
				<img class="mx-auto d-block" src="images/logo_.png" height="200px"/>

					<nav class="navbar navbar-expand-lg navbar-light m-b-30">
					  <div class="container-fluid">
					    <a class="navbar-brand" href="#"><?php echo $user['user_name'];?></a>
					    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					      <span class="navbar-toggler-icon"></span>
					    </button>
					    <div class="collapse navbar-collapse" id="navbarNav">
					      <ul class="navbar-nav">
					        <li class="nav-item">
					          <a class="nav-link active" aria-current="page" href="#">Mes Badges</a>
					        </li>
					        <li class="nav-item">
					          <a class="nav-link" href="?logout=true">Deco</a>
					        </li>

					      </ul>
					    </div>
					  </div>
					</nav>


					<?php
					$query=$bdd->prepare('SELECT * FROM qr_code WHERE code_owner_id = :cv');
					$query->bindValue(':cv',$user['user_id'], PDO::PARAM_STR);
					$query->execute();
					while($code=$query->fetch()) {
						echo '
						<div class="container-login100-form-btn p-b-10">
							<div class="wrap-login100-form-btn">
								<div class="login100-form-bgbtn"></div>
								<a class="login100-form-btn" href="'.$code['code_url'].'">
									'.$code['code_name'].'
								</a>
							</div>
						</div>
						';
					}


					 ?>


					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary float-end m-t-20" data-bs-toggle="modal" data-bs-target="#exampleModal">
						+
					</button>

					<!-- Modal -->
					<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Nouveau QR Code</h5>
									<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
								</div>
								<div class="modal-body">
									<form id="new_qr_form" class="login100-form validate-form" action="" method="POST">

										<div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
											<span class="label-input100">Nom de l'objet</span>
											<input class="input100" type="text" name="object_QR_code_name" placeholder="Entrez le nom de l'objet">
											<span class="focus-input100" data-symbol="&#xf206;"></span>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
									<button  form="new_qr_form" type="submit" class="btn btn-primary">Ajouter le QR Code</button>
								</div>
							</div>
						</div>
					</div>



			</div>
		</div>
	</div>




<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>-->
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
